//▸ Реализовать структуру IOSCollection и создать в ней copy on write
import Foundation

func address (of object: UnsafeMutablePointer<Any>) -> String {
    let addr = Int(bitPattern: object)
    return String(format: "%p", addr)
}

func address (off value: AnyObject) -> String {
    return "\(Unmanaged.passUnretained(value).toOpaque())"
}



struct IosCollection {
    var id = 2
}

class Ref<T> {
    var value: T
    init (value: T) {
        self.value = value
    }
}

struct Container<T> {
    var ref: Ref<T>
    init(value: T) {
        self.ref = Ref(value: value)
    }
    
    var value: T {
        get {
            ref.value
        }
        set{
            guard (isKnownUniquelyReferenced(&ref)) else {
                ref = Ref(value: newValue)
                return
            }
            ref.value = newValue
        }
    }
}

var id = IosCollection()
var container1 = Container(value: id)
var container2 = container1

address(off: container1.ref)
address(off: container2.ref)

container2.value.id = 12

address(off: container1.ref)
address(off: container2.ref)

//Cоздать протокол *Hotel* с инициализатором, который принимает roomCount, после создать class HotelAlfa добавить свойство roomCount и подписаться на этот протокол

protocol Hotel {
    init (roomCount: Int)
    
}

class HotelAlfa: Hotel{
    
    let roomCount: Int
    
    required init(roomCount: Int) {
        self.roomCount = roomCount
    }
    
}
let hotel = HotelAlfa(roomCount: 23)

//Создать protocol GameDice у него {get} свойство numberDice далее нужно расширить Int так, чтобы когда мы напишем такую конструкцию 'let diceCoub = 4 diceCoub.numberDice' в консоле мы увидели такую строку - 'Выпало 4 на кубике'

protocol GameDice {
    var numberDice: String {get}
}

extension Int: GameDice {
    var numberDice: String {
        return "Выпало\(self) на кубике"
    }
}

let diceCoub = 4
diceCoub.numberDice

//Создать протокол с одним методом и 2 свойствами одно из них сделать явно optional, создать класс, подписать на протокол и реализовать только 1 обязательное свойство

@objc protocol Worker {
    var profession: String {get}
    @objc optional var jobTitle: String {get}
    
    func profAndExp()
}

class Engineer: Worker {
    var profession: String
    
    init(profession:String){
        self.profession = profession
    }
    
    func profAndExp() {
        print("Profession is \(profession)")
    }
}

let human = Engineer(profession: "Shipbuilder")
human.profAndExp()

//Создать 2 протокола: со свойствами время, количество кода и функцией writeCode(platform: Platform, numberOfSpecialist: Int); и другой с функцией: stopCoding(). Создайте класс: Компания, у которого есть свойства - количество программистов, специализации(ios, android, web)
//▸ Компании подключаем два этих протокола
//▸ Задача: вывести в консоль сообщения - 'разработка началась. пишем код <такой-то>' и 'работа закончена. Сдаю в тестирование', попробуйте обработать крайние случаи.
enum Platform: String{
    case ios
    case web
    case android
}

protocol Work {
    var time: Int {get}
    var amount: Int {get}
    func writeCode(platform: Platform, numberOfSpecialist: Int)
}

protocol Stop {
    func stopCoding()
}

class Company {
    var amountCoders: Int
    var specialization: Platform
    
    init(amountCoders: Int, specialization: Platform){
        self.amountCoders = amountCoders
        self.specialization = specialization
    }
}

var company1 = Company(amountCoders: 3, specialization: .ios)

extension Company: Work {
    var time: Int {
        return 7
    }
    
    var amount: Int {
        return 700
    }
    
    func writeCode(platform: Platform, numberOfSpecialist: Int) {
        print("Разработка началась. Пишем под \(platform.rawValue). Кол-во специалиство: \(numberOfSpecialist)")
    }
}

extension Company: Stop {
    func stopCoding() {
        print("Работа закончена. Сдаю в тестирование")
    }
}

company1.writeCode(platform: .ios, numberOfSpecialist: 5)
company1.stopCoding()
